# Template

![Version](https://img.shields.io/badge/dynamic/json?color=informational&label=version&query=%24.general.version&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![License](https://img.shields.io/badge/dynamic/json?color=green&label=license&query=%24.general.license&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/SW_LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Description



### Working principle



## Current state

The project is currently at a very early stage of development, so it is likely that most things are not working.

## Using



## Dependencies

- [lib](link) \[1.x.x\] - description

## Contributing

Contributions are welcome!

## License

The source code is licensed under the [MIT](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/LICENSE) license.
